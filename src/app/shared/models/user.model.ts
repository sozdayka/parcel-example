
export interface IUserModel {
    name: string;
    email: string;
    img?: string;
}