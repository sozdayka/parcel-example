# how install parcel
    npm i -g parcel-bundler
# how generation packege.json
    npm init
# how install node-sass (for scss)
    npm i node-sass

# how run 
    * parcel index.html
    or
    * npm start (if add on the packege.json this: 
                                                "scripts": {
                                                    "test": "echo \"Error: no test specified\" && exit 1",
                                                    "start": "parcel index.html"
                                                },
                )



