import { IUserModel } from "../../shared/models/user.model";

export class LocalStorageService{
    constructor(){}

    getUser(): IUserModel[]{
        return localStorage.getItem('users') ? JSON.parse(localStorage.getItem('users')) as IUserModel[] : [];
    }

    addUser(array:IUserModel[]): void{
        localStorage.setItem('users',JSON.stringify(array));
    }

    updateUser(array:IUserModel[]): void{
        localStorage.setItem('users',JSON.stringify(array));
    }

    removeUser(array:IUserModel[]): void{
        localStorage.setItem('users',JSON.stringify(array));
    }
}