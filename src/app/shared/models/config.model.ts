export interface IConfigModel {
    APP_ENV?: string,
    DB_HOST?: string,
    DB_USER?: string,
    DB_PASS?: string,
};