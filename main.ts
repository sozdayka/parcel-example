import './src/assets/scss/main.scss';
import { UserService } from './src/app/shared/services/user.services';
import { ApiConfig } from './src/app/config/api.config';


let User: UserService = new UserService();
let btn = document.getElementById("btnAddUser") as HTMLElement;
let uploadImg = document.getElementById("fileUser") as HTMLFormElement;
let previewUploadImg = document.getElementById("previeFileUser") as HTMLFormElement;

console.log(ApiConfig.DB_HOST);

class Main{
    constructor() {
        previewUploadImg.setAttribute("style", "display: none");
        this.init();       
    }

    init() {
        let userImageBase64:string = '';
        btn.onclick = () => {
            if(btn.classList.contains('btn-uploading')){
                alert("Upload image plz!!!");
                return;
            }
            this.addUser(userImageBase64);
        };

        
        uploadImg.onclick = () => {
            btn.classList.add('btn-uploading');
        };
        
        uploadImg.onchange = (event) => {
            var reader = new FileReader();
                reader.readAsDataURL(uploadImg.files[0]);
                reader.onload = function(){
                    userImageBase64 = reader.result.toString();
                    previewUploadImg.setAttribute("style", "display: block");
                    previewUploadImg.setAttribute("src", userImageBase64);
                    btn.classList.remove('btn-uploading');
                };
        };
        btn.classList.remove('btn-uploading');
    }
    addUser(image: string) {
        User.addUser(image);
    }
}

new Main();



