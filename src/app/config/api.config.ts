import * as process from 'process';
import * as dotenv from 'dotenv';
import { IConfigModel } from '../shared/models/config.model';

let fs = require('fs');
let NODE_ENV = process.env.NODE_ENV || 'dev';
let getEnvFromFile:IConfigModel = dotenv.parse(fs.readFileSync(`./src/app/utils/.env.${NODE_ENV}`,'utf-8'));

export const ApiConfig:IConfigModel = {
    APP_ENV: getEnvFromFile.APP_ENV,
    DB_HOST: getEnvFromFile.DB_HOST,
    DB_USER: getEnvFromFile.DB_PASS,
    DB_PASS: getEnvFromFile.DB_USER,
}
