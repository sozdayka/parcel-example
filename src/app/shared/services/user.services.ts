import { IUserModel } from "../../shared/models/user.model";
import { LocalStorageService } from "./localstorage.service";

export class UserService {
    listUser: IUserModel[] = [];

    private localStorageService: LocalStorageService;
    constructor(){
        this.localStorageService = new LocalStorageService();
        this.listUser = this.getAllUsers();
        this.drowUsers();
    }

    getAllUsers() : IUserModel[] {
        return this.localStorageService.getUser();
    }

    addUser(imageUpload: string) : void {
        let nameInput: HTMLFormElement = document.getElementById('nameUser') as HTMLFormElement;
        let emailInput: HTMLFormElement = document.getElementById('emailUser') as HTMLFormElement;

        let user: IUserModel = {
            name: nameInput.value,
            email: emailInput.value,
            img: imageUpload
        };
        this.listUser.push(user);
        this.localStorageService.addUser(this.listUser);
        alert('User Added!');
        nameInput.value = '';
        emailInput.value = '';

        this.drowUsers();
    }
    
    updateUser(parentHtmlElemnt: HTMLElement, user: IUserModel) : void {
        let nameUser: HTMLFormElement = parentHtmlElemnt.getElementsByClassName('nameUser')[0] as HTMLFormElement;
        let emailUser: HTMLFormElement = parentHtmlElemnt.getElementsByClassName('emailUser')[0] as HTMLFormElement;

        let foundUserIndexInArray = this.listUser.indexOf(user);
        if(this.listUser.indexOf(user) != -1){
            this.listUser[foundUserIndexInArray].name = nameUser.value;
            this.listUser[foundUserIndexInArray].email = emailUser.value;
            this.localStorageService.updateUser(this.listUser);
            alert('User Updated!');
        }else{
            alert("Server Error: User not found");
        }

    }

    removeUser(user: IUserModel) : void {
        let foundUserIndexInArray = this.listUser.indexOf(user);

        if(this.listUser.indexOf(user) != -1){
            this.listUser.splice(foundUserIndexInArray,1);
            this.localStorageService.removeUser(this.listUser);
            alert('User Removed!');
            this.drowUsers();
        }else{
            alert("Server Error: User not found");
        }

    }

    drowUsers() : void {
        let mainDiv: HTMLElement = document.getElementById('ulist') as HTMLFormElement;
        mainDiv.innerHTML = "";
            
        if(this.listUser.length == 0){
            mainDiv.innerHTML = "<b>Sory, users array is empty</b>";
        }

        for (let user of this.listUser) {
            let elementDiv: HTMLElement = document.createElement("div");
            let elementInputName: HTMLElement = document.createElement("input");
            let elementInputEmail: HTMLElement = document.createElement("input");
            let elementImage: HTMLElement = document.createElement("img");
            let elementButtonSave: HTMLElement = document.createElement("button");
            let elementButtonRemove: HTMLElement = document.createElement("button");

            elementImage.setAttribute("style", "width: 50px;");
            elementImage.setAttribute("src", user.img);
            elementInputName.setAttribute("type", "text");
            elementInputName.setAttribute("class", "nameUser");
            elementInputName.setAttribute("value", user.name);
            elementInputEmail.setAttribute("type", "text");
            elementInputEmail.setAttribute("class", "emailUser");
            elementInputEmail.setAttribute("value", user.email);

            elementButtonSave.innerHTML = "Save";
            elementButtonSave.setAttribute("class", 'btn-save');
            elementButtonSave.onclick = (event) => {
                let _thisBtn = event.target as HTMLElement
                this.updateUser(_thisBtn.parentElement, user);
            };
            elementButtonRemove.innerHTML = "x";
            elementButtonRemove.setAttribute("class", 'btn-remove');
            elementButtonRemove.onclick = () => {
                this.removeUser(user);
            };

            elementDiv.appendChild(elementImage);
            elementDiv.appendChild(elementInputName);
            elementDiv.appendChild(elementInputEmail);
            elementDiv.appendChild(elementButtonSave);
            elementDiv.appendChild(elementButtonRemove);

            mainDiv.appendChild(elementDiv);
        }

    }
}