'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function () {
    console.log('satt')
    return gulp.src('src/assets/scss/main.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest('src/assets/css'))
});

gulp.task('watch', ['sass'], function () {
    gulp.watch('src/assets/scss/**/*.scss', ['sass']);
});


